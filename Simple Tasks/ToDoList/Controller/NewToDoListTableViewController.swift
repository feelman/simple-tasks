//
//  NewToDoListTableViewController.swift
//  Simple Tasks
//
//  Created by Konstantin Filobok on 13.08.2020.
//  Copyright © 2020 Konstantin Filobok. All rights reserved.
//

import UIKit
import CoreData

class NewToDoListTableViewController: UITableViewController {
    
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func saveButtonAction() {
        
        guard nameTextView.text != "" else { return }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let newItem = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context)
        
        newItem.setValue(nameTextView.text, forKey: "name")
        newItem.setValue(UUID(), forKey: "id")
        
        do {
            try context.save()
        } catch {
            print("error")
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("newTask"), object: nil)
    }
    
}
