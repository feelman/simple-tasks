//
//  ToDoListViewController.swift
//  Simple Tasks
//
//  Created by Konstantin Filobok on 05.05.2020.
//  Copyright © 2020 Konstantin Filobok. All rights reserved.
//

import UIKit
import CoreData

class ToDoListViewController: UITableViewController {
    
    var toDoArr: [String] = []
    var toDoIDArr: [UUID] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Light Tasks"
        
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(getData), name: NSNotification.Name("newTask"), object: nil)
    }
    
    @objc func getData() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                self.toDoArr.removeAll(keepingCapacity: false)
                self.toDoIDArr.removeAll(keepingCapacity: false)
            }
            for result in results as! [NSManagedObject] {
                if let name = result.value(forKey: "name") as? String {
                    toDoArr.insert(name, at: 0)
                }
                if let id = result.value(forKey: "id") as? UUID {
                    toDoIDArr.insert(id, at: 0)
                }
            }
            tableView.reloadData()
        } catch {
            print("error")
        }
        
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        guard segue.identifier == "saveSegue" else { return }
        let sourceVC = segue.source as! NewToDoListTableViewController
        sourceVC.saveButtonAction()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return toDoArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "toDoCell", for: indexPath) as! ToDoListTableViewCell
        cell.nameLabel.text = toDoArr[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        
        let idStr = toDoIDArr[indexPath.row].uuidString
        
        request.predicate = NSPredicate(format: "id = %@", idStr)
        
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if let id = result.value(forKey: "id") as? UUID {
                        if id == toDoIDArr[indexPath.row] {
                            context.delete(result)
                            toDoArr.remove(at: indexPath.row)
                            toDoIDArr.remove(at: indexPath.row)
                            self.tableView.reloadData()
                        }
                        do {
                            try context.save()
                        } catch {
                            print("error")
                        }
                        break
                    }
                }
            }
        } catch {
            print("error")
        }
    }
}
