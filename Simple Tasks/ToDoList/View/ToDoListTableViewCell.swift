//
//  ToDoListTableViewCell.swift
//  Simple Tasks
//
//  Created by Konstantin Filobok on 08.05.2020.
//  Copyright © 2020 Konstantin Filobok. All rights reserved.
//

import UIKit

class ToDoListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!

}
